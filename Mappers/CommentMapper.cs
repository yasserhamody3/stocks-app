using api.DTOs.Comment;
using api.Models;

namespace api.Mappers
{
    public static class CommentMapper
    {
        public static CommentDto ToCommentDTO(this Comment CommentModel)
        {
            return new CommentDto
            {
                Content = CommentModel.Content,
                CreatedOn = CommentModel.CreatedOn,
                Id = CommentModel.Id,
                StockId = CommentModel.StockId,
                Title = CommentModel.Title
            };
        }

        public static Comment ToCommentfromCreate(this CreateCommentDTO commentDTO, int StockId)
        {
            return new Comment
            {
                Content = commentDTO.Content,
                Title = commentDTO.Title,
                StockId = StockId
            };
        }
    }
}
