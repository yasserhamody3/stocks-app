using api.DTOs.Stock;
using api.Models;
using Microsoft.AspNetCore.Mvc;

namespace api.Mappers
{
    public static class StockMappers
    {
        public static StockDto ToStockDTo(this Stock StockModel)
        {
            return new StockDto
            {
                CompanyName = StockModel.CompanyName,
                id = StockModel.id,
                Industry = StockModel.Industry,
                LastDiv = StockModel.LastDiv,
                MarketCap = StockModel.MarketCap,
                purchase = StockModel.purchase,
                symbol = StockModel.symbol ,
                Comments = StockModel.Comments.Select(s=>s.ToCommentDTO()).ToList()
            };
        }

        public static Stock ToStockFromCreateDTo(this CreateStockRequestDTo stockDTO)
        {
            return new Stock
            {
                symbol = stockDTO.symbol,
                purchase = stockDTO.purchase,
                Industry = stockDTO.Industry,
                CompanyName = stockDTO.CompanyName,
                LastDiv = stockDTO.LastDiv,
                MarketCap = stockDTO.MarketCap
            };
        }
       
    }
}
