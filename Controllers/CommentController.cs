using api.Data;
using api.DTOs.Comment;
using api.Interfaces;
using api.Mappers;
using api.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata;

namespace api.Controllers
{
    [Route("api/comment")]
    [ApiController]
    public class CommentController : ControllerBase
    {
        private readonly ICommentRepo _CommentRepo;
        private readonly IstockRepo _StockRepo;

        public CommentController(ICommentRepo comment, IstockRepo stockRepo)
        {
            _CommentRepo = comment;
            _StockRepo = stockRepo;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var comments = await _CommentRepo.GetAll();
            var CommentsDTO = comments.Select(s => s.ToCommentDTO());
            return Ok(CommentsDTO);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> getByID([FromRoute] int id)
        {
            var comment = await _CommentRepo.getByID(id);
            if (comment == null)
            {
                return NotFound();
            }
            return Ok(comment.ToCommentDTO());
        }

        [HttpPost("{id}")]
        public async Task<IActionResult> createComment(
            [FromRoute] int id,
            CreateCommentDTO commentDTO
        )
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var stock = await _StockRepo.IsStockExist(id);
            if (!stock)
            {
                return BadRequest("The Stock is not found");
            }
            var commentModel = commentDTO.ToCommentfromCreate(id);
            await _CommentRepo.CreateComment(id, commentModel);
            return CreatedAtAction(
                nameof(getByID),
                new { id = commentModel },
                commentModel.ToCommentDTO()
            );
        }

        [HttpPut]
        [Route("{id}")]
        public async Task<IActionResult> UpdateComment(
            [FromRoute] int id,
            [FromBody] UpdateCommentDTO commentToUpdate
        )
        {
            var comment = await _CommentRepo.UpdateComment(id, commentToUpdate);

            if (null == comment)
            {
                return NotFound("The comment is not exits");
            }

            return Ok(comment.ToCommentDTO());
        }

        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> DeleteComment([FromRoute] int id)
        {
            var CommentToDelte = await _CommentRepo.DeleteComment(id);
            if (null == CommentToDelte)
            {
                return NotFound("The comment is not found");
            }
            return NoContent();
        }
    }
}
