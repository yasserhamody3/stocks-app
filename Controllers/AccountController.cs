using api.DTOs.Accounts;
using api.Interfaces;
using api.Models;
using api.Services;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace api.Controllers
{
    [Route("api/account")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly ITokenService _token;

        public AccountController(UserManager<AppUser> usermanager, ITokenService token)
        {
            _userManager = usermanager;
            _token = token;
        }

        [HttpPost("register")]
        public async Task<IActionResult> createAccount([FromBody] RegisterDTo registerRequest)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var appUser = new AppUser
                {
                    UserName = registerRequest.UserName,
                    Email = registerRequest.Email , 
                    
                };
                
                var user = await _userManager.CreateAsync(appUser, registerRequest.PassWord);

                if (user.Succeeded)
                {
                    var addRoles = await _userManager.AddToRoleAsync(appUser, "user");
                    if (addRoles.Succeeded)
                    {
                        var response = new RegisterResponseDTO
                        {
                            Email = appUser.Email,
                            Token = _token.CreateToken(appUser),
                            UserName = appUser.UserName
                        };
                        return Ok(response);
                    }
                    else
                    {
                        return StatusCode(500, addRoles.Errors);
                    }
                }
                else
                {
                    return StatusCode(500, user.Errors);
                }
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }
    }
}
