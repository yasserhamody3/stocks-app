using api.Data;
using api.DTOs.Stock;
using api.Helpers;
using api.Interfaces;
using api.Mappers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace api.Controllers
{
    [Route("api/stock")]
    [ApiController]
    public class StockController : ControllerBase
    {
        private readonly IstockRepo _stockRepo;

        public StockController(IstockRepo stockrepo)
        {
            _stockRepo = stockrepo;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] SearchQuery searchQuery)
        {
            var founded_stocks = await _stockRepo.getAllAsync(searchQuery);
            var stocks = founded_stocks.Select(s => s.ToStockDTo());
            return Ok(stocks);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetByID([FromRoute] int id)
        {
            var stock = await _stockRepo.GetByID(id);

            if (stock == null)
            {
                return NotFound();
            }

            return Ok(stock.ToStockDTo());
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CreateStockRequestDTo stockdto)
        {
             if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var StockModel = stockdto.ToStockFromCreateDTo();
            await _stockRepo.CreateStockAsync(StockModel);
            return CreatedAtAction(
                nameof(GetByID),
                new { id = StockModel.id },
                StockModel.ToStockDTo()
            );
        }

        [HttpPut]
        [Route("{id}")]
        public async Task<IActionResult> Update(
            [FromRoute] int id,
            [FromBody] UpdateStockRequestDTo updatededDTO
        )
        {
            var StockModel = await _stockRepo.UpdateStock(id, updatededDTO);

            if (StockModel == null)
            {
                return NotFound();
            }

            return Ok(StockModel.ToStockDTo());
        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult Delete([FromRoute] int id)
        {
            var StockModel = _stockRepo.DeleteStock(id);

            if (StockModel == null)
            {
                return NotFound();
            }
            return NoContent();
        }
    }
}
