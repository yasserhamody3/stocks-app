using System.ComponentModel.DataAnnotations;

namespace api.DTOs.Stock
{
    public class CreateStockRequestDTo
    {
        public string CompanyName { get; set; } = string.Empty;

        public string symbol { get; set; } = string.Empty;
        [Required]
        [Range(2,1000000000)]
        public decimal purchase { get; set; }

        public decimal LastDiv { get; set; }

        public string Industry { get; set; } = string.Empty;

        public long MarketCap { get; set; }
    }
}