using System.ComponentModel.DataAnnotations;
using api.DTOs.Comment;

namespace api.DTOs.Stock
{
    public class StockDto
    {
        public int id { get; set; }
        public string CompanyName { get; set; } = string.Empty;

        public string symbol { get; set; } = string.Empty;
        [Required]
        [Range(1,1000000000)]
        public decimal purchase { get; set; }

        public decimal LastDiv { get; set; }

        public string Industry { get; set; } = string.Empty;

        public long MarketCap { get; set; }
        public List<CommentDto > Comments {get; set; }
    }
}
