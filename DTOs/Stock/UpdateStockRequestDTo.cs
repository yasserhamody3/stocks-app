namespace api.DTOs.Stock
{
    public class UpdateStockRequestDTo
    {
        public string CompanyName { get; set; } = string.Empty;

        public string symbol { get; set; } = string.Empty;
        public decimal purchase { get; set; }

        public decimal LastDiv { get; set; }

        public string Industry { get; set; } = string.Empty;

        public long MarketCap { get; set; }
    }
}
