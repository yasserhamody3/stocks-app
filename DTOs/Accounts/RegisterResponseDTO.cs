namespace api.DTOs.Accounts
{
    public class RegisterResponseDTO
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }
    }
}
