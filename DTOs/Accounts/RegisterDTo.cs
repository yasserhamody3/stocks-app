using System.ComponentModel.DataAnnotations;

namespace api.DTOs.Accounts
{
    public class RegisterDTo
    {
        [Required]
        public string? UserName { get; set; }

        [Required]
        [EmailAddress]
        public string? Email { get; set; }

        [Required]
        public string? PassWord { get; set; }
    }
}
