namespace api.DTOs.Comment
{
    public class UpdateCommentDTO
    {
        public string title { get; set; } = string.Empty;
        public string content { get; set; } = string.Empty;
    }
}
