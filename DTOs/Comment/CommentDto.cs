using System.ComponentModel.DataAnnotations;

namespace api.DTOs.Comment
{
    public class CommentDto
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(100, ErrorMessage = "The Max Length of Chars must be one hundred")]
        [MinLength(5, ErrorMessage = "The Min Message of Chars must be five")]
        public string Title { get; set; } = string.Empty;

        public string Content { get; set; } = string.Empty;

        public int? StockId { get; set; }

        public DateTime CreatedOn { get; set; } = DateTime.Now;
    }
}
