using api.DTOs.Comment;
using api.Models;

namespace api.Interfaces
{
    public interface ICommentRepo
    {
        public Task<List<Comment>> GetAll();
        public Task<Comment?> getByID(int id);
        public Task<Comment> CreateComment(int id, Comment commentModel);
        public Task<bool> IsCommentExist(int id);
        public Task<Comment?> DeleteComment(int id);
        public  Task<Comment?> UpdateComment(
            int id,
            UpdateCommentDTO commentToUpdate
        );
    }
}
