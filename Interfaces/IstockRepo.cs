using api.DTOs.Stock;
using api.Helpers;
using api.Models;

namespace api.Interfaces
{
    public interface IstockRepo
    {
        Task<List<Stock>> getAllAsync(SearchQuery query);
        Task<Stock?> UpdateStock(int id, UpdateStockRequestDTo UpdateStockDTo);
        Task<Stock> CreateStockAsync(Stock StockDTo);
        Task<Stock?> GetByID(int id);
        Task<Stock?> DeleteStock(int id);
        Task<bool> IsStockExist(int id);
    }
}
