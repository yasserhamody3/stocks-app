using api.Data;
using api.DTOs.Comment;
using api.Interfaces;
using api.Mappers;
using api.Models;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace api.Repository
{
    public class CommentRepo : ICommentRepo
    {
        private readonly ApplicationDBContext _Context;

        public CommentRepo(ApplicationDBContext context)
        {
            _Context = context;
        }

        public async Task<Comment> CreateComment(int id, Comment commenModel)
        {
            await _Context.Comments.AddAsync(commenModel);
            await _Context.SaveChangesAsync();
            return commenModel;
        }

        public async Task<Comment?> DeleteComment(int id)
        {
            var comment = await _Context.Comments.FirstOrDefaultAsync(x => x.Id == id);

            if (null == comment)
            {
                return null;
            }
            _Context.Comments.Remove(comment);
            await _Context.SaveChangesAsync();

            return comment;
        }

        public async Task<List<Comment>> GetAll()
        {
            return await _Context.Comments.ToListAsync();
        }

        public async Task<Comment?> getByID(int id)
        {
            return await _Context.Comments.FindAsync(id);
        }

        public async Task<bool> IsCommentExist(int id)
        {
            return await _Context.Comments.AnyAsync(i => i.Id == id);
        }

        public async Task<Comment?> UpdateComment(
            int id,
            UpdateCommentDTO commentToUpdate
        )
        {
         
                var foundedComment = await _Context.Comments.FindAsync(id);
                if (foundedComment == null)
                {
                    return null;
                }

                foundedComment.Content = commentToUpdate.content;
                foundedComment.Title = commentToUpdate.title;
                await _Context.SaveChangesAsync();
                return foundedComment;
            
        }
    }
}
