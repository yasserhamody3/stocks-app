using api.Data;
using api.DTOs.Stock;
using api.Helpers;
using api.Interfaces;
using api.Mappers;
using api.Models;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.EntityFrameworkCore;

namespace api.Repository
{
    public class StockRepo : IstockRepo
    {
        private readonly ApplicationDBContext _context;

        public StockRepo(ApplicationDBContext context)
        {
            _context = context;
        }

        public async Task<List<Stock>> getAllAsync(SearchQuery query)
        {
            var skipedValue = (query.PageNumber - 1) * (query.PageSize);
            var stocks = _context.Stock.Include(c => c.Comments).AsQueryable();

            if (!string.IsNullOrWhiteSpace(query.CompanyName))
            {
                stocks = stocks.Where(s => s.CompanyName.Contains(query.CompanyName));
            }
            if (!string.IsNullOrWhiteSpace(query.symbol))
            {
                stocks = stocks.Where(s => s.symbol.Contains(query.symbol));
            }
            if (!string.IsNullOrWhiteSpace(query.SortBy))
            {
                if (query.SortBy.Equals("symbol", StringComparison.OrdinalIgnoreCase))
                {
                    stocks = query.IsDescending
                        ? stocks.OrderByDescending(s => s.symbol)
                        : stocks.OrderBy(s => s.symbol);
                }
                else if (query.SortBy.Equals("CompanyName", StringComparison.OrdinalIgnoreCase))
                {
                    stocks = query.IsDescending
                        ? stocks.OrderByDescending(s => s.CompanyName)
                        : stocks.OrderBy(s => s.CompanyName);
                }
            }

            return await stocks.Skip(skipedValue).Take(query.PageSize).ToListAsync();
        }

        public async Task<Stock> CreateStockAsync(Stock StockDTo)
        {
            await _context.Stock.AddAsync(StockDTo);
            await _context.SaveChangesAsync();
            return StockDTo;
        }

        public async Task<Stock?> GetByID(int id)
        {
            return await _context.Stock
                .Include(c => c.Comments)
                .FirstOrDefaultAsync(i => i.id == id);
        }

        public async Task<Stock?> UpdateStock(int id, UpdateStockRequestDTo updateStock)
        {
            var FoundedStock = await _context.Stock.FirstOrDefaultAsync(x => x.id == id);
            if (FoundedStock == null)
            {
                return null;
            }

            FoundedStock.purchase = updateStock.purchase;
            FoundedStock.MarketCap = updateStock.MarketCap;
            FoundedStock.symbol = updateStock.symbol;
            FoundedStock.Industry = updateStock.Industry;
            FoundedStock.CompanyName = updateStock.CompanyName;
            FoundedStock.purchase = updateStock.purchase;

            await _context.SaveChangesAsync();
            return FoundedStock;
        }

        public async Task<Stock?> DeleteStock(int id)
        {
            var FoundedStock = await _context.Stock.FirstOrDefaultAsync(x => x.id == id);
            if (FoundedStock == null)
            {
                return null;
            }
            _context.Stock.Remove(FoundedStock);
            await _context.SaveChangesAsync();
            return FoundedStock;
        }

        public async Task<bool> IsStockExist(int id)
        {
            return await _context.Stock.AnyAsync(i => i.id == id);
        }
    }
}
